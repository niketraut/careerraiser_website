
<!--<?php
  //  $description="Add current page description";
  //  $keywords="Add current page keyward";
    $title="Certification";

?>-->
<?php include 'include-top-header.php'; ?>
   <?php include 'include-header.php'; ?>

       <?php include'include-page-title.php'; ?>

        <!--
        ==================================================
            Service Page Section  Start
        ================================================== -->
        <section id="service-page" class="pages service-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">What We Provide</h2>
                            <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms">We offer following industry oriented trainings and certifications.
                              <br> and needless to say all our trainers are industry veterans. They bring to table not only the subject matter expertise but also their years of experience that gives you knowledge much beyond the subject.</p>
                            <div class="row service-parts">
                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms">
                                      <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms">
                                        <ol style="list-style-type:disc;">
                                      <li style="text-align:left; font-weight: bold;"> TOGAF </li>
                                      <li style="text-align:left; font-weight: bold;"> Project Management Certification </li>
                                      <li style="text-align:left; font-weight: bold;"> Agile SCRUM Master </li>
                                      <li style="text-align:left; font-weight: bold;"> Project Management - Prince2 Practitioner </li>
                                      <li style="text-align:left; font-weight: bold;"> Quality Improvement - LEAN Six sigma</li>
                                      <li style="text-align:left; font-weight: bold;"> IT Process Management - ITIL Expert</li>
                                    </ol>
                                      </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <img class="img-responsive" src="images/portfolio/Certification.jpg" alt="">
                        </div>
                    </div>
                    <?php // TODO: Add right side bar ?>
                    <?php /*<div class="col-md-3">
                        <?php include'include-right-side-bar.php' ?>
                    </div> */ ?>
                </div>
                <hr style="border-width:1px;">

                <div class="row" style="text-align:center;">
                  <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">Our USP</h2>
                  <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms">We understand the need behind these certifications. We won't stop just at giving you training.</p>

                    <div class="col-md-3 block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms" style="border: solid black 2px; border-color:grey;">
                      <p style="font-weight: bold;"> Free lecture on how to enhance your career further (individual specific guidance) </p>
                    </div>
                  <div class="col-md-3 block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms" style="border: solid black 2px;">
                    <p style="font-weight: bold;"> resume writing guidance. We will also review your current resume and advise on how to improve it. </p>
                  </div>
                    <div class="col-md-3 block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms" style="border: solid black 2px; border-color:grey;">
                      <p style="font-weight: bold;"> Free access to future lectures in case you want to brush up your concepts </p>
                    </div>
                  <div class="col-md-3 block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms" style="border: solid black 2px;">
                    <p style="font-weight: bold;"> In case you need to switch, we will help you there as well through our past candidate network</p>
                  </div>
                </div>
                <hr style="border-width:1px;">

              </div>
        </section>


        <!--
        ==================================================
            Works Section Start
        ================================================== -->
        <section class="works service-page">
            <div class="container">
              <div class="row" style="text-align:center;">
                <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">Global Certifications</h2>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms">
                        International Certifications to boost your career and improve job prospects .
                    </p>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                          <div class="blog-post-image">
                              <img class="img-responsive" src="images/certification/ITIL Certification1.jpg" alt="ITIL" />
                          </div>
                          <div class="blog-content">
                              <h2 class="blogpost-title">ITIL Certification</h2>
                              <p>The ITIL - Foundation certification builds standard terminology and focuses on the methodology for ITIL services support and services delivery. This helps candidates in understanding what exactly is ITIL and how it is used in the workplace.
                              </p>
                              </p>
                              <p><br></p>
                            <p style="display:inline-block;"><a href="Syllabus/Management/ITIL Foundation_Introduction.pdf" target="_blank" class="btn btn-dafault btn-details">Download</a></p>
                              <?php /*<p>ITIL Intermediat Syllabus <a href="Syllabus/Management/ITIL Intermediate.pdf" class="btn btn-dafault btn-details">Download</a></p>*/ ?>
                          </div>
                      </article>

                    </div>


                    <div class="col-md-6" style="background-color:#3a8899; color: white;">
                      <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                          <div class="blog-post-image">
                              <img class="img-responsive" src="images/certification/Prince2.jpg" alt="Prince2" />
                          </div>
                          <div class="blog-content">
                              <h2 class="blogpost-title">
                              Prince2
                              </h2>
                              <?php /*<div class="blog-meta">
                                  <span>Dec 11, 2020</span>
                                  <span>by <a href="">Admin</a></span>
                                  <span><a href="">business</a>,<a href="">people</a></span>
                              </div> */ ?>

                              <p>There are three levels of PRINCE2 certification: Foundation, Practitioner and Professional, which are aimed at project managers and those who are involved throughout the planning and delivering projects.
                                 These three qualifications examine the PRINCE2 methodology in increasing depth and offer individuals and organizations the ability to implement PRINCE2 to suit their specific needs.
                              </p>
                              <a href="Syllabus/Management/Prince2_Introduction-1.pdf" target="_blank" class="btn btn-dafault btn-details">Download</a>
                          </div>
                      </article>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-6" style="background-color:#3a8899; color: white;">
                        <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                            <div class="blog-post-image">
                                <img class="img-responsive" src="images/certification/project-management.png" alt="PMP" />
                            </div>
                            <div class="blog-content">
                                <h2 class="blogpost-title">
                               Project Management Certification
                                </h2>
                                <?php /*  <div class="blog-meta">
                                      <span>Dec 11, 2020</span>
                                      <span>by <a href="">Admin</a></span>
                                      <span><a href="">business</a>,<a href="">people</a></span>
                                  </div> */ ?>

                                  <p>When you earn a PMI certification, you show peers, supervisors and clients your commitment to the profession, PMI's code of ethics and your ability to perform the function of a project management practitioner to a certain level.
                                  </p>
                                <p><br></p>
                                <a href="Syllabus/Management/PMP_Introduction.pdf" target="_blank" class="btn btn-dafault btn-details">Download</a>
                            </div>
                        </article>

                      </div>

                      <div class="col-md-6">
                        <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                            <div class="blog-post-image">
                                <img class="img-responsive" src="images/certification/six sigma.jpg" alt="LEAN Six sigma" />
                            </div>
                            <div class="blog-content">
                                <h2 class="blogpost-title">
                                LEAN Six sigma
                                </h2>
                                <?php /*<div class="blog-meta">
                                    <span>Dec 11, 2020</span>
                                    <span>by <a href="">Admin</a></span>
                                    <span><a href="">business</a>,<a href="">people</a></span>
                                </div>
                                 */ ?>

                                <p>Certified LEAN Six sigma is a professional who can explain Six Sigma philosophies and principles, including supporting systems and tools. A Black Belt should demonstrate team leadership,
                                   understand team dynamics and assign team member roles and responsibilities.
                                </p>
                                <a href="Syllabus/Management/SSBB_Introduction.pdf" target="_blank" class="btn btn-dafault btn-details">Download</a>
                            </div>
                        </article>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                          <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                              <div class="blog-post-image">
                                  <img class="img-responsive" src="images/certification/TOGAF.jpg" alt="TOGAF" />
                              </div>
                              <div class="blog-content">
                                  <h2 class="blogpost-title">
                                  TOGAF (The Open Group Architecture Framework)
                                  </h2>
                                  <?php /*  <div class="blog-meta">
                                        <span>Dec 11, 2020</span>
                                        <span>by <a href="">Admin</a></span>
                                        <span><a href="">business</a>,<a href="">people</a></span>
                                    </div> */ ?>

                                    <p>TOGAF takes a high-level approach to the framework that an enterprise uses to plan, design, implement, and manage its Enterprise Architecture.
                                       Enterprise architecture is broken down into four distinct domains (data, technology, application, and business) and heavily relies on existing, standardized products and technologies.
                                    </p>
                                  <a href="Syllabus/Management/Togaf_Introduction.pptx" target="_blank" class="btn btn-dafault btn-details">Download</a>
                              </div>
                          </article>

                        </div>

                        <div class="col-md-6" style="background-color:#3a8899; color: white;">
                          <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                              <div class="blog-post-image">
                                  <img class="img-responsive" src="images/certification/Software Testing.jpg" alt="ISTQB" />
                              </div>
                              <div class="blog-content">
                                  <h2 class="blogpost-title">
                                  ISTQB (International Software Testing Qualifications Board)
                                  </h2>
                                  <?php /*<div class="blog-meta">
                                      <span>Dec 11, 2020</span>
                                      <span>by <a href="">Admin</a></span>
                                      <span><a href="">business</a>,<a href="">people</a></span>
                                  </div>
                                   */ ?>

                                   <p>The Foundation Level qualification is aimed at professionals who need to demonstrate practical
                                      knowledge of the fundamental concepts of software testing. This includes people in roles such as test designers, test analysts, test engineers, test consultants, test managers, user acceptance testers and IT Professionals.
                                   </p>
                                  <a href="Syllabus/Management/Togaf_Introduction.pptx" target="_blank" class="btn btn-dafault btn-details">Download</a>
                              </div>
                          </article>
                        </div>
                      </div>
                  </div>
            </div>
        </section>


                <!--
                ==================================================
                    Call To Action Section Start
                ================================================== -->

              <?php include'vendor.php' ?>

              <!--
              ==================================================
                  Call To Action Section End
              ================================================== -->




              <?php include 'include-above-footer.php';?>

<?php include 'include-footer.php';?>

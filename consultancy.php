<?php
  //  $description="Add current page description";
  //  $keywords="Add current page keyward";
    $title="Consultancy";

?>

<?php include 'include-top-header.php'; ?>
   <?php include 'include-header.php'; ?>

        <?php include'include-page-title.php'; ?>

        <!--
        ==================================================
            Service Page Section  Start
        ================================================== -->
        <section id="service-page" class="pages service-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">IT Consultant</h2>
                               <div class="row service-parts">

                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms">


                                      <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms" style="text-align:left;">
                                            Consultants provide an analysis of the existing practices of a company and make recommendations for improvements. These professionals frequently specialize in one area of business management,
                                          such as human resources. For example, a hospital may hire a healthcare business consultant to help further develop its employee training programs or a distribution center may hire a logistics business consultant to streamline its shipping department.
                                      </p>

                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <img class="img-responsive" src="images/portfolio/Cunsultant.jpg" alt="">
                        </div>
                    </div>
                    <?php // TODO: Add right side bar ?>
                    <?php /*<div class="col-md-3">
                        <?php include'include-right-side-bar.php' ?>
                    </div> */ ?>
                </div>
              </div>
        </section>




        <section id="blog-full-width">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">
                        <div class="sidebar">

                                <div class="recent-post widget" style="background-color:#eeeeee;">
                                    <h3 style="color:blue;">Application development services</h3>
                                    <ul>
                                        <li>
                                            <span class="glyphicon glyphicon-globe"></span> Get your software developed as per your requirements<br>
                                        </li>
                                        <li>
                                            <span class="glyphicon glyphicon-globe"></span> You can also speed up process by adopting our ready solutions<br>
                                        </li>
                                        <li>
                                            <span class="glyphicon glyphicon-globe"></span> Solutions for all budgets<br>
                                        </li>
                                        <li><br></li>
                                        <li><br></li>
                                    </ul>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class ="sidebar">
                              <img class="img-responsive, img-thumbnail" src="images/consultancy/software-development.jpg" alt="Application development">
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                              <div class="sidebar">
                                  <div class="recent-post widget" style="background-color:#eeeeee;">
                                      <h3 style="color:blue;">Software solution architecture services</h3>
                                          <ul>
                                              <li>
                                                <span class="glyphicon glyphicon-globe"></span> Already have your software development team?<br>
                                              </li>
                                              <li>
                                                <span class="glyphicon glyphicon-globe"></span> Get the solutions designed from our industry experienced software architects<br>
                                              </li>
                                              <li>
                                                <span class="glyphicon glyphicon-globe"></span> Tap into MNC knowledge experience of our certified architects<br>
                                              </li>
                                              <li><br></li>
                                          </ul>
                                      </div>
                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class ="sidebar">
                                    <img class="img-responsive, img-thumbnail" src="images/consultancy/architecture.jpg" alt="Software Architecture">
                                  </div>
                                </div>
                              </div>


                              <div class="row">
                                <div class="col-md-4" style="display:inline;">
                                    <div class="sidebar">

                                            <div class="recent-post widget" style="background-color:#eeeeee;">
                                                <h3 style="color:blue;">Process automation services</h3>

                                                <ul>
                                                    <li>
                                                        <span class="glyphicon glyphicon-globe"></span> Save on your operational costs by automating your processes<br>

                                                    </li>
                                                    <li>
                                                        <span class="glyphicon glyphicon-globe"></span> Consult our Industry experiences professional to how you can gain from the automation<br>

                                                    </li>
                                                    <li>
                                                        <span class="glyphicon glyphicon-globe"></span> You can automate daily routine activities done via software and reduce human resources<br>

                                                    </li>
                                                    <li>
                                                      <span class="glyphicon glyphicon-globe"></span> You can also automate your manufacturing and other processes
                                                      <br>

                                                    </li>

                                                </ul>

                                            </div>

                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class ="sidebar">
                                    <img class="img-responsive, img-thumbnail" src="images/consultancy/Automation4.jpg" alt="Automation">
                                  </div>
                                </div>
                              </div>



                              <div class="row">
                                <div class="col-md-4" style="display:inline;">
                                    <div class="sidebar">

                                            <div class="recent-post widget" style="background-color:#eeeeee;">
                                                <h3 style="color:blue;">Process value and efficiency studies</h3>

                                                <ul>
                                                    <li>
                                                      <span class="glyphicon glyphicon-globe"></span> We can study your processes and point out improvement and automation needs<br>
                                                    </li>
                                                    <li>
                                                      <span class="glyphicon glyphicon-globe"></span> Through our six sigma certified consultants.<br>
                                                    </li>
                                                    <li>
                                                    <span class="glyphicon glyphicon-globe"></span> Get a constraint analysis done for your process and discover new ways to increase revenue
                                                        <br>
                                                    </li>
                                                    <li><br></li>

                                              </ul>

                                            </div>

                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class ="sidebar">
                                    <img class="img-responsive, img-thumbnail" src="images/consultancy/process value.jpg" alt="Process value">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-4" style="display:inline;">
                                    <div class="sidebar">

                                            <div class="recent-post widget" style="background-color:#eeeeee;">
                                                <h3 style="color:blue;">Employee enhancement services</h3>

                                                <ul>
                                                    <li>
                                                         <span class="glyphicon glyphicon-globe"></span> Consult our industry experienced HR professionals to get a through analysisdone of your workforce<br>
                                                    </li>
                                                    <li>
                                                        <span class="glyphicon glyphicon-globe"></span> Identify hidden potentials of your staff and enhance it through trainings<br>
                                                    </li>
                                                    <li>
                                                        <span class="glyphicon glyphicon-globe"></span> Groom them for next level of leadership<br>
                                                    </li>
                                                    <li>
                                                       <span class="glyphicon glyphicon-globe"></span> Identify soft skill training needs of your staff accurately<br>
                                                    </li>
                                                </ul>

                                            </div>

                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class ="sidebar">
                                    <img class="img-responsive, img-thumbnail" src="images/consultancy/hr.jpg" alt="Employee enhancement">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-4" style="display:inline;">
                                    <div class="sidebar">

                                            <div class="recent-post widget" style="background-color:#eeeeee;">
                                                <h3 style="color:blue;">New technology PoC</h3>

                                                <ul>
                                                  <li>
                                                      <span class="glyphicon glyphicon-globe"></span> Thinking of trying out that new cutting edge technology?<br>
                                                  </li>
                                                   <li>
                                                      <span class="glyphicon glyphicon-globe"></span> Before spending on it, once check with our experts<br>
                                                  </li>
                                                   <li>
                                                       <span class="glyphicon glyphicon-globe"></span> We may already have done something about it.<br>
                                                   </li>
                                                   <li>
                                                       <span class="glyphicon glyphicon-globe"></span> Gain from our experience and experiments.<br>
                                                   </li>
                                                  </ul>

                                            </div>

                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class ="sidebar">
                                    <img class="img-responsive, img-thumbnail" src="images/consultancy/blockchain.jpg" alt="New technology">
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>
                </section>





        <!--
        ==================================================
            Vendor Section Start
        ================================================== -->

      <?php include'vendor.php' ?>

      <!--
      ==================================================
          Vendor Section End
      ================================================== -->



      <?php include 'include-above-footer.php';?>

<?php include 'include-footer.php';?>

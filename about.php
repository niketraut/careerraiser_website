<?php
  //  $description="Add current page description";
  //  $keywords="Add current page keyward";
    $title="About Us";
    
?>
<?php include 'include-top-header.php'; ?>
   <?php include 'include-header.php'; ?>

        <?php include'include-page-title.php' ?>

        <!--
        ==================================================
            Company Description Section Start
        ================================================== -->
        <section class="company-description">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/portfolio/aboutus.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Why We are Different</h3>
                            <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                              CareerRaiser.In is a training institute based in Thane, Mumbai.
                              We are a group of working professionals who conduct training and other consulting activities in spare time.
                              because of our experience, we bring a unique perspective to training by adding practical experiences, tips and advises etc. Currently, we are authorized training center for ITIL (IT Infrastructure Libraries),
                              Prince2 (International Software Testing Qualifications Board), TOGAF and Resilia.
                            </p>
                            <p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                              One of our major focus areas is to bridge the gap between current industry requirement and college syllabus through Fresher Training Program. We offer complete training including soft skills to fresh college passout so that they can face interviews and clear them with full confidence.
                              we also offer them complete placement assistance till they get a job in the industry.

                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--
        ==================================================
            Company Payment Link
        ================================================== -->


      <?php include'include-payment-link.php'; ?>


        <!--
        ==================================================
            Company Feature Section Start
        ================================================== -->
        <section class="about-feature clearfix" style="margin-top: 0px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="block about-feature-1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">
                        <h2>
                        Why Choose Us
                        </h2>
                        <p>
                          For instance, we only use industry experienced and qualified tutor and assessors and use a wide of learning.
                          <br>The very easy booking process with helpful guidance.
                          <br>Course content very good and professionally delivered. Attendees made to feel at ease in a friendly environment, making participation easy.
                          <br>Feedback from operatives is they provide faultless services and are very thorough.<br>
                        </p>
                    </div>
                    <div class="block about-feature-2 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                        <h2 class="item_title">
                        What You Get
                        </h2>
                        <p>
                               CareerRaiser offers students a great opportunity to gain the skills needed to enter into a new career.
                              <br> Many fast-track options are also available in these types of environments, allowing the student to focus on learning the primary knowledge base required for success.
                              <br>How to maximize your technical knowledge base to enter into a new career.
                          </p>
                    </div>
                    <div class="block about-feature-3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".7s">
                        <h2 class="item_title">
                        Meet The Future
                        </h2>
                        <p>
                          With the explosion of the <b>Internet of Things</b>, the number of devices connected to the Internet will increase from around 13 billion today to 50 billion in 2020.
                          The IoT is a rapidly evolving wave of technology.
                          <br> More and more technologies are integrated with it and it’s fast developing into a network where absolutely everything is connected; from wind turbines to home appliances.
                       </p>
                    </div>
                </div>
            </div>
        </section>





                <!--
                ==================================================
                    Call To Action Section Start
                ================================================== -->

              <?php include'vendor.php' ?>

              <!--
              ==================================================
                  Call To Action Section End
              ================================================== -->

      <?php include 'include-above-footer.php';?>
  <?php include 'include-footer.php';?>
